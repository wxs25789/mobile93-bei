// 这个位置放置用户相关的方法
// 导入requset（封装好的axios）
import request from '@/utils/request.js'

// 封装登录方法且导出出去
export function apiUserLogin ({ mobile, code }) {
  return request({
    url: '/app/v1_0/authorizations',
    method: 'POST',
    data: {
      mobile,
      code
    }
  })
}
