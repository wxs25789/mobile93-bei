// 这个位置配置axios
import axios from 'axios'
import JSONBig from 'json-bigint'

// 导入store
import store from '@/store/index.js'

// 导入路由完成编程式导航
import router from '@/router/index.js'

// 创建一个新的axios对象
const instance = axios.create({
  // 请求根地址
  baseURL: 'http://ttapi.research.itcast.cn/',
  // 配置超大整形数字
  transformResponse: [function (data) {
    try {
      return JSONBig.parse(data)
    } catch (e) {
      return data
    }
  }]
})
// 请求拦截器
instance.interceptors.request.use(function (config) {
  if (store.state.user.token) {
    config.headers.Authorization = 'Bearer' + store.state.user.token
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

// 响应拦截器
instance.interceptors.response.use(function (response) {
  // 返回成功的信息
  try {
    return response.data.data
  } catch (e) {
    return response.data
  }
}, function (error) {
  // 返回错误的信息
  // 如果返回的值是错误的，且返回的状态码是401
  if (error.response.status === 401) {
    // 跳转到登录页面
    router.push('/login')
    // 返回一个空的promise对象，不会在做任意错误操作了
    return new Promise(function () {})
  }
  // 如果返回的错误状态码不是401，就返回错误信息
  return Promise.reject(error)
})

// 导出新的axios对象
export default instance
