// 这个位置是校验工具
// 导入一个注册的方法,且导入语言包
import { extend, localize } from 'vee-validate'
// 导入所有的校验规则
import * as rules from 'vee-validate/dist/rules'

// 导入需要使用的语言包
import zhCN from 'vee-validate/dist/locale/zh_CN'

// 注册语言包
localize({ zhCN })

// 默认使用语言包
localize('zhCN')
// 将rules规则对象集合返回一个数组，进行遍历
Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule])
})
// 自定义手机号校验规则
extend('phone', {
  validate: value => {
    const reg = /^1[35789]\d{9}$/
    return reg.test(value)
  },
  message: '{_field_}格式不正确'
})
